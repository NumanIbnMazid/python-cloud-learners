# def function_name(argument):
#     # function body
    # return

def hello():
    # function body
    string = "Hello world!"
    # print(string)
    return string

# call function
# function_name(argument)
result = hello()
print(result)

# operations with result

def calculate1():
    return 10 + 13

def calculate2():
    return 50 + 50

result = calculate1() + calculate2()

print(result)


my_string1 = "Hello "

def my_string():
    my_string2 = "World"
    # print(my_string1)
    # print(my_string2)
    result = my_string1 + my_string2
    return result


print(my_string())

def my_func():
    pass
    # do some work

def my_func2():
    pass
    # do otehr work

def another_func():
    result = 100 + 200
    print(result)
    return result

another_func()


# def func_name(parameter/argument):
#     function body

def hello(sentence):
    return sentence

print(hello("Hello world"))




def operation3(operation1, operation2):
    result = operation1 + operation2
    return result


# operation1 = 23  # background -> some work
# operation2 = 1000  # background -> some work

print(operation3(23, 1000))

print("\n My name function => ")

"""
My Comment
My Comment 2
"""
# my comment 1
# my comment 2
print("Hello \n \t hello \n \t \t hello")

print("""Hello World
Hello world
    Hello world
        Hello world""")

# File 1 
def my_name(last_name, first_name):
    """ my_name(last_name, first_name)
    This function does some work"""
    full_name = first_name + " " + last_name
    return full_name

# File 2
print(my_name(last_name="Doe", first_name="John"))


print("\n Multiplier Function =>")

def multiplier(num1, num2):
    return num1 * num2

print(multiplier(num1=10, num2=20))


print("\n Square Function =>")

def square(num):
    """takes 1 argument, square function squares a number and return the result"""
    # result = num * num
    return int(num) * int(num)

print(square(6))

def default_value(num1=1, num2=1):
    return num1 + num2

print(default_value())