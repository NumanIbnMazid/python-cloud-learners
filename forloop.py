print("For loop from list => \n")

my_list = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

for num in my_list:
    print("Number => ", num)

numbers = [(1,2), (3,4), (5,6)]

print("\n Tuple unpacking line =>")

# Tuple unpacking
for (a, b) in numbers:
    print(a)
    print(b)

print("\n For loop in dictionary =>")
my_dict = {'key1': 1, 'key2': 2, 'key3': 3}

# Print just key
for item in my_dict:
    print(item)

# Print key, value
for key, value in my_dict.items():
    print(f"Key is {key}, value is {value}")
    # print(key)
    # print(value)

# Print just value
for v in my_dict.values():
    print(v)


# 1- 10 with range
var1= 1
var2 = 10
for num in range(var1, var2 + 2):
    print(num)

nums = []
for num in range(10, 51):
    if num % 5 == 0:
        nums.append(num)

for num in range(10, 51, 5):
    nums.append(num)

print(nums)

# print(30 % 12)