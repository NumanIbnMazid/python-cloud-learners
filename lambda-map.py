def square(num):
    return num * num, num

# print(square(4))

my_nums = [1, 2, 3, 4, 5, 6]

for item in map(square, my_nums):
    print(item)


# lambda function => Anonymous function
# lambda argument: method body
num_square = lambda *args: args[0] - args[1]
print(num_square(4, 2))
