# Syntex => string.split('Split Symbol')
c = "Hello-World"

c = c.split('-')
print(c)

# syntex => 'separator of list element'.join(list_name)
x = '?'.join(c)

print(x)

# syntex => string.replace('old one', 'new one')
my_string = x.replace('?', ' ')
print(my_string)
 











# Question 1:
 
# ANIMAL CRACKERS: Write a function takes a two-word string and returns True if both words begin with same letter
 
# animal_crackers('Levelheaded Llama') - -> True
# animal_crackers('Crazy Kangaroo') - -> False

# function => Two word string => return True : 1st characeter of first word == 1st character of second word

def animal_crackers(text):
    text = text.split(' ')
    return text[0][0] == text[1][0]


print(animal_crackers("Tofael Aahmod"))

# Task Convert this function to lambda

# One Way
string = "Tofael Tahmod"
string = string.split(' ')
result = lambda txt: txt[0][0] == txt[1][0]
print(result(string))

# Another way
result = lambda txt: txt.split(' ')[0][0] == txt.split(' ')[1][0]
print(result("Tofael Tahmod"))

# function call => function_name(argument)

print("\n Question: 2 => makes_twenty \n")
# Question 2:

# MAKES TWENTY: Given two integers, return True if the sum of the integers is 20 or if one of the integers is 20. If not, return False

# makes_twenty(20, 10) - -> True
# makes_twenty(12, 8) - -> True
# makes_twenty(2, 3) - -> False

def makes_twenty(n1, n2):
    return n1 + n2 == 20 or 20 in [n1, n2]


print(makes_twenty(20, 6))


# Question 3:

# OLD MACDONALD: Write a function that capitalizes the first and fourth letters of a name¶

# old_macdonald('macdonald') - -> MacDonald
# Note: 'macdonald'.capitalize() returns 'Macdonald'

print("\n Question: 3 => old_macdonald \n")

def old_macdonald(name):
    first = name.split(name[3:])
    second = name.split(name[:3])
    return first[0].capitalize() + second[1].capitalize()


print(old_macdonald('macdonald'))


print("\n Question: 4 => master_yoda \n")

# Question 4:

# MASTER YODA: Given a sentence, return a sentence with the words reversed

# master_yoda('I am home') - -> 'home am I'
# master_yoda('We are ready') - -> 'ready are We'


def master_yoda(text):
    words = text.split(" ")
    words.reverse()
    return " ".join(words)


print(master_yoda('I am home'))


print("\n Question: 5 => paper_doll \n")
# Question 5:

# PAPER DOLL: Given a string, return a string where for every character in the original there are three characters

# paper_doll('Hello') - -> 'HHHeeellllllooo'
# paper_doll('Mississippi') - -> 'MMMiiissssssiiissssssppppppiii'


def paper_doll(text):
    result = []
    letter_list = list(text)
    for letter in letter_list:
        result.append(letter * 3)

    return "".join(result)


print(paper_doll('Mississippi'))


print("\n Question: 6 => spy_game \n")
# Question 6:

# SPY GAME: Write a function that takes in a list of integers and returns True if it contains 007 in order

# spy_game([1, 2, 4, 0, 0, 7, 5]) - -> True
# spy_game([1, 0, 2, 4, 0, 5, 7]) - -> True
# spy_game([1, 7, 2, 0, 4, 5, 0]) - -> False


def spy_game(nums):
    check_list = []
    for num in nums:
        if num in (0,7):
            check_list.append(str(num))
    return "".join(check_list) == "007"


print(spy_game([1, 2, 4, 0, 0, 7, 5]))


print("\n Question: 7 => up_low \n")

# Question 7:

# Write a Python function that accepts a string and calculates the number of upper case letters and lower case letters.

# Sample String: 'Hello Mr. Rogers, how are you this fine Tuesday?'
# Expected Output:
# No. of Upper case characters: 4
# No. of Lower case Characters: 33
# HINT: Two string methods that might prove useful: .isupper() and .islower()


def up_low(string):
    uppers = []
    lowers = []
    for s in string:
        if s.isupper():
            uppers.append(s)
        if s.islower():
            lowers.append(s)
    return f"No. of Upper case characters: {len(uppers)} \n No. of Lower case Characters: {len(lowers)}"

print(up_low('Hello Mr. Rogers, how are you this fine Tuesday?'))


print("\n Question: 8 => palindrome \n")
 # Question 8:

# Write a Python function that checks whether a passed in string is palindrome or not.

# Note: A palindrome is word, phrase, or sequence that reads the same backward as forward, e.g., madam or nurses run.


def palindrome(string):
    string = string.replace(" ", "")
    return string == string[::-1]


print(palindrome('nurses run'))
