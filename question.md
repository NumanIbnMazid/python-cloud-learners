 # Question 1:
 
 ANIMAL CRACKERS: Write a function takes a two-word string and returns True if both words begin with same letter
 
animal_crackers('Levelheaded Llama') - -> True
animal_crackers('Crazy Kangaroo') - -> False


 # Question 2:
 
 MAKES TWENTY: Given two integers, return True if the sum of the integers is 20 or if one of the integers is 20. If not, return False
 
makes_twenty(20, 10) - -> True
makes_twenty(12, 8) - -> True
makes_twenty(2, 3) - -> False


 # Question 3:
 
OLD MACDONALD: Write a function that capitalizes the first and fourth letters of a name¶

old_macdonald('macdonald') - -> MacDonald
Note: 'macdonald'.capitalize() returns 'Macdonald'


 # Question 4:
 
MASTER YODA: Given a sentence, return a sentence with the words reversed

master_yoda('I am home') - -> 'home am I'
master_yoda('We are ready') - -> 'ready are We'


 # Question 5:

PAPER DOLL: Given a string, return a string where for every character in the original there are three characters

paper_doll('Hello') - -> 'HHHeeellllllooo'
paper_doll('Mississippi') - -> 'MMMiiissssssiiippppppiii'


 # Question 6:
 
SPY GAME: Write a function that takes in a list of integers and returns True if it contains 007 in order

spy_game([1, 2, 4, 0, 0, 7, 5]) - -> True
spy_game([1, 0, 2, 4, 0, 5, 7]) - -> True
spy_game([1, 7, 2, 0, 4, 5, 0]) - -> False


 # Question 7:
 
Write a Python function that accepts a string and calculates the number of upper case letters and lower case letters.

Sample String: 'Hello Mr. Rogers, how are you this fine Tuesday?'
Expected Output:
No. of Upper case characters: 4
No. of Lower case Characters: 33
HINT: Two string methods that might prove useful: .isupper() and .islower()
 
 
  # Question 8:
 
Write a Python function that checks whether a passed in string is palindrome or not.

Note: A palindrome is word, phrase, or sequence that reads the same backward as forward, e.g., madam or nurses run.