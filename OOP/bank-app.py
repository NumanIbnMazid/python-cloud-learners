from abc import ABC, abstractmethod

class BankAccount(ABC):
    __balance = 0

    def deposit(self, amount):
        self.__balance += amount

    def withdraw(self, amount):
        if self.__balance >= amount:
            self.__balance -= amount
        else:
            print("Not enough balance!")

    def get_balance(self):
        return self.__balance

    @abstractmethod
    def interest(self):
        pass

    @abstractmethod
    def expense(self):
        pass

class CurrentAccount(BankAccount):
    def interest(self):
        self.deposit(100)

    def expense(self):
        self.withdraw(1000)

class SavingsAccount(BankAccount):
    def interest(self):
        self.deposit(120)

    def expense(self):
        self.withdraw(500)

# a1 = BankAccount()

# print(a1.get_balance())

# a1.deposit(1000)

# print(a1.get_balance())

c1 = CurrentAccount()

c1.deposit(1000)

print(c1.get_balance())
c1.interest()
print(c1.get_balance())
