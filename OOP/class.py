class Computer:
    __color = "Blue"
    def input(self):
        print("Can take input")

    def display(self):
        print("has a display")

    def get_color(self):
        return self.__color

    def set_color(self, color):
        self.__color = color

c1 = Computer()

c1.set_color("Red")

print(c1.get_color())

class Laptop(Computer):
    def fold(self):
        print("Can be folded")


class Mobile(Laptop):
    pass

l1 = Laptop()

# l1.input()

m1 = Mobile()

m1.input()
m1.fold()

class A:
    def color(self):
        print("Blue")

a1 = A()

print(A.color(a1))



