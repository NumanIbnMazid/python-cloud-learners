
# Blueprint
class Employee:
    # Constructor/Initialization
    def __init__(self, first_name, last_name, email):
        # Instance Variable
        self.first_name = first_name
        self.last_name = last_name
        self.email = email

    def hello(self):
        print("Hello World")

    def get_fullname(self):
        return f"{self.first_name} {self.last_name}"


# Instance/Object
emp_1 = Employee("Atiq", "Ashik", "ashiq@gmail.com")
emp_2 = Employee("Tofayel", "Ahammod", "tofayel@gmail.com")

# emp_1.hello()

# print(id(emp_1))
# print(id(emp_2))

# emp_1.first_name = "Atiq"
# emp_1.last_name = "Ashik"
# emp_1.email = "ashiq@gmail.com"

# emp_2.first_name = "Tofayel"
# emp_2.last_name = "Ahammod"
# emp_2.email = "tofayel@gmail.com"

print(emp_1.first_name)
print(emp_2.first_name)

print(emp_2.get_fullname())
