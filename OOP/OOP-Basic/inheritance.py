
# Super class
class A:
    def feature1(self):
        print("Feature 1 working")

    def feature2(self):
        print("Feature 2 working")

# Child class
# Single level inheritance
class B(A):
    def feature3(self):
        print("Feature 3 working")

    def feature4(self):
        print("Feature 4 working")

a1 = A()
b1 = B()

print(isinstance(b1, A))
print(issubclass(B, A))

a1.feature1()
b1.feature1()

class X:
    def featureX(self):
        print("Form class X")

class Y:
    def featureY(self):
        print("Form class Y")

# Multiple inheritance
class Z(X, Y):
    pass

z1 = Z()

z1.featureX()
z1.featureY()

# print("Check Subclass", issubclass(Z, X))
# print("Check Subclass", issubclass(Z, Y))


print("\n Animal Inheritance \n")


class Animal():
    def __init__(self):
        print("Animal Created")

    def who_am_i(self):
        return "I am an Animal"

    def eat(self):
        return "I am eating"


class Dog(Animal):
    def who_am_i(self):
        return "I am an Dog"
        

class Cat(Animal):
    def who_am_i(self):
        return "I am an Cat"

dog = Dog()
cat = Cat()

print(dog.eat())
print(cat.eat())
print(dog.who_am_i())
print(cat.who_am_i())
