# Poly Morph ism

"""
Polymorphism:
    - Duck Typing
    - Operator Overloading
    - Method Overloading
    - Method Overriding
"""

"""
Duck Typing
"""

class Vscode:
    def excute(self):
        print("Compilling")
        print("Running")

class MyIDE:
    def excute(self):
        print("Spell Check")
        print("Convention Check")
        print("Compilling")
        print("Running")

class Laptop:
    def code(self, ide):
        ide.excute()


ide1 = MyIDE()
ide2 = Vscode()
lap1 = Laptop()

lap1.code(ide1)

# Only concern : IDE Must have execute method


"""
Operator Overloading
"""

a = 5
b = 6

print(str.__add__("10", "13"))

class Student:
    def __init__(self, m1, m2):
        self.m1 = m1
        self.m2 = m2

    # Magic Methods
    def __add__(self, other):
        m1 = self.m1 + other.m1
        m2 = self.m2 + other.m2
        s3 = Student(m1, m2)
        return s3

    def __sub__(self, other):
        m1 = self.m1 - other.m1
        m2 = self.m2 - other.m2
        s3 = Student(m1, m2)
        return s3

    def __gt__(self, other):
        r1 = self.m1 + self.m2
        r2 = other.m1 + other.m2
        if r1 > r2:
            return True
        return False

s1 = Student(80, 40)
s2 = Student(50, 30)

s3 = s1 + s2
s3 = s1 - s2
print(s3.m2)

if s1 > s2:
    print("s1 wins")
else:
    print("s2 wins")


"""
Method Overloading:
Python does not support method overloading by default. But can be done with tricks.
"""


class Student:
    def __init__(self, m1, m2):
        self.m1 = m1
        self.m2 = m2

    # def get_name(self, value: int):
    #     print(value)
    #     print(type(value))

    # def get_name(self, value: str):
    #     print(value)
    #     print(type(value))

    # def get_name(self, value: str, value2: int):
    #     print(value)
    #     print(value2)
    #     print(type(value))
    #     print(type(value2))

    def sum(self, a=None, b=None, c=None):
        s = 0
        if a != None and b != None and c != None:
            s = a + b + c
        elif a != None and b != None:
            s = a + b
        else:
            s = a
        return s


s1 = Student(45, 78)

# print(s1.get_name(23))
# print(s1.get_name("XXX"))
# print(s1.get_name(23, "XXX"))

print(s1.sum(12, 10, 13))
print(s1.sum(12, 10))
print(s1.sum(12))


"""
Method Overriding
    - Must be inheritance
    - Both class methods have same name, same argument
"""

class A:
    def show(self):
        print("In A Show")

class X:
    def show(self):
        print("In X Show")

# MRO => Method Resolution Order: Left - Right
class B(X, A):
    # def show(self):
    #     print("In B Show")
    pass

b1 = B()
b1.show()

print(B.mro())