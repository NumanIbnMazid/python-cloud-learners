# https://docs.python.org/3/reference/datamodel.html

class Employee:
    raise_amount = 20.04
    # initialization / constructor

    def __init__(self, first_name, last_name, email, pay):
        # instance variable
        self.first_name = first_name
        self.last_name = last_name
        self.email = email
        self.pay = pay

    def get_fullname(self):
        # return f"{self.first_name} {self.last_name}"
        return "{} {}".format(self.first_name, self.last_name)

    def apply_raise(self):
        self.pay = int(self.pay + self.raise_amount)

    # Unambigous representation of the object, Used for debugging, logging
    def __repr__(self):
        return "Employee('{}', '{}', '{}', '{}')".format(self.first_name, self.last_name, self.email, self.pay)

    # Readable representation of an object to end user
    def __str__(self):
        return "{} - {}".format(self.get_fullname(), self.email)

    # def __add__(self, other):
    #     pass

    # def __mul__(self, other):
    #     pass

    # def __len__(self):
    #     pass


emp_1 = Employee("Tofayel", "Ahammok", "tofayel@gmail.com", 49000)

print(emp_1)

print(emp_1.__repr__())
print(emp_1.__str__())

print(repr(emp_1))
print(str(emp_1))
