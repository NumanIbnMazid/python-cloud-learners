

class Car:
    def __init__(self):
        self.__updateSoftware()

    def drive(self):
        print("Driving")

    def __updateSoftware(self):
        print("Updating software")

redcar = Car()

class Car:
    __maxspeed = 200
    __name = ""

    def __init__(self):
        self.__maxspeed = 200
        self.__name = "Supercar"

    def drive(self):
        print(f"Driving . Maxspeed {str(self.__maxspeed)}")


redcar = Car()
redcar.drive()
redcar.__maxspeed = 10 # Will not change
redcar.drive()

# If want to change private variables


class Car:
    __maxspeed = 200
    __name = ""

    def __init__(self):
        self.__maxspeed = 200
        self.__name = "Supercar"

    def drive(self):
        print(f"Driving . Maxspeed {str(self.__maxspeed)}")

    def setMaxSpeed(self, value):
        self.__maxspeed = value

    def getMaxSpeed(self):
        return self.__maxspeed


redcar = Car()
redcar.drive()
redcar.setMaxSpeed(10)
redcar.drive()

print(redcar.getMaxSpeed())
