

class Student:
    def __init__(self, name, rollno, brand, cpu, ram):
        self.name = name
        self.rollno = rollno
        self.laptop = self.Laptop(brand, cpu, ram)

    def show(self):
        print(f"Name: {self.name}, Roll no: {self.rollno}")
        self.laptop.show()

    class Laptop:
        def __init__(self, brand, cpu, ram):
            self.brand = brand
            self.cpu = cpu
            self.ram = ram
        
        def show(self):
            print(f"Brand: {self.brand}, CPU: {self.cpu}, RAM: {self.ram}")


s1 = Student("John Doe", 2, "HP", "i5", 16)
s2 = Student("Steve Smith", 1, "Lenovo", "i7", 12)


s1.show()
s2.show()