"""
Python doesn't support abstraction directly. But can be done with the help of ABC module.
"""

from abc import ABC, abstractmethod

class Computer(ABC):

    @abstractmethod
    def process(self):
        pass

class Laptop(Computer):
    def process(self):
        print("It's processing")

# com = Computer()
# We cannot create any instance of abstract class

lap = Laptop()

lap.process()
