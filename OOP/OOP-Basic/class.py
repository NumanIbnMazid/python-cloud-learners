
# Blueprint
class Employee:
    # Class variable
    num_of_emps = 0
    raise_amount = 20.04
    # initialization / constructor
    def __init__(self, first_name, last_name, email, pay):
        # instance variable
        self.first_name = first_name
        self.last_name = last_name
        self.email = email
        self.pay = pay

    def get_fullname(self):
        # return f"{self.first_name} {self.last_name}"
        return "{} {}".format(self.first_name, self.last_name)

    def apply_raise(self):
        self.pay = int(self.pay + self.raise_amount)


# instance/Object
emp_1 = Employee("Rabbee", "Ahmed", "rabee@gmail.com", 50000)
emp_2 = Employee("Tofayel", "Ahammok", "tofayel@gmail.com", 49000)

# print(id(emp_1))
# print(id(emp_2))

# emp_1.first_name = "Rabbee"
# emp_1.last_name = "Ahmed"
# emp_1.email = "rabee@gmail.com"

# emp_2.first_name = "Tofayel"
# emp_2.last_name = "Ahammok"
# emp_2.email = "tofayel@gmail.com"

# print(emp_1.first_name + " " + emp_1.last_name)

# print(emp_2.first_name + " " + emp_2.last_name)

print(emp_2.get_fullname())


# emp_1.get_fullname()
# Employee.get_fullname(emp_2)

print(emp_1.pay)
emp_1.apply_raise()
print(emp_1.pay)


print(Employee.raise_amount)

Employee.raise_amount = 50.05

print(Employee.raise_amount)

print(emp_1.__dict__)
print(Employee.__dict__)


emp_1.raise_amount = 80.04

print(Employee.raise_amount)
