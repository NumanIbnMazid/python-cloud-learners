class Employee:
    num_of_emps = 0
    raise_amount = 20.04

    def __init__(self, first_name, last_name, email, pay):
        # instance variable
        self.first_name = first_name
        self.last_name = last_name
        self.email = email
        self.pay = pay

    def get_fullname(self):
        return "{} {}".format(self.first_name, self.last_name)

    def apply_raise(self):
        self.pay = int(self.pay + self.raise_amount)

    @classmethod
    def set_raise_amt(cls, amount):
        cls.raise_amount = amount

    @classmethod
    def from_string(cls, emp_str):
        first_name, last_name, email, pay = emp_str.split("-")
        return cls(first_name, last_name, email, pay)

    @staticmethod
    def is_workday(day):
        # Monday = 0, Sunday = 6
        if day.weekday() == 5 or day.weekday() == 6:
            return False
        return True


emp_1 = Employee("Rabbee", "Ahmed", "rabee@gmail.com", 50000)
emp_2 = Employee("Tofayel", "Ahammok", "tofayel@gmail.com", 49000)

print(Employee.raise_amount)
Employee.set_raise_amt(30.04)
print(Employee.raise_amount)

emp_str_1 = "John-Doe-john@gmail.com-10000"
emp_str_2 = "Steve-Smith-steve@gmail.com-15000"


new_emp_1 = Employee.from_string(emp_str_1)
new_emp_2 = Employee.from_string(emp_str_2)

print(new_emp_1.email)
print(new_emp_2.get_fullname())


import datetime
my_date = datetime.date(2020, 8, 22)
print(Employee.is_workday(my_date))
