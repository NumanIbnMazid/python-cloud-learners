def hello(*args):
    # 100 * (100 + 25)
    # 100 * (100 + 25) = 12500
    
    return args[0] * (args[0] + args[1])


print(hello(100, 25, 100, 20))

# args, kwargs

# args => treat as a tuple
# kwargs =>  treats as key value / dictionary

def my_func(*args):
    return args


print(my_func(10, 20, 30, 40, 10, 20, 30, 40))

def my_func2(**kwargs):
    return kwargs

print(my_func2(one='Banana', two='Apple', three='Lichi'))


def my_func3(*text):
    return text

print(my_func3("Hello", "John"))


def sum_nums(*args):
    return args


print(sum_nums(10, 20, 23, 12, 10, 20, 23, 12, 10, 20, 23, 12, 10, 20, 23,
               12, 10, 20, 23, 12, 10, 20, 23, 12, 10, 20, 23, 12, 10, 20, 23, 12))

def my_func_5(**kwargs):
    return kwargs['one']


print(my_func_5(one="Noyon", two="Asib", three="Torun", four="Tofu"))


print("\n Args Kwargs => \n")

def kwargsfunc(**kwargs):
    print(kwargs)

    if 'fruit' in kwargs:
        return f"My fruit of choice is {kwargs['fruit']}"
    return "I didn't find any fruit here."


print((kwargsfunc(fruit='apple', flower='rose', sports='football')))
