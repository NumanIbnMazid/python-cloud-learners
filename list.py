list1 = [1,2,3,4,5]  # list
print(list1)

list2 = [1, "String", False, 1.5]
print(list2)

# indexing
# variabale_name[index_position]

print(list2[1])
print(list2[-1], list2[3])
print(list2[-4])

list3 = list1 + list2
print(list3)
print(list3[6:9] * 2)

print(len(list3))

print(list3)
# [1, 2, 3, 4, 5, 1, 'String', False, 1.5]  => List 3

# list_name.append(object)
list3.append("Tofayel")

print(list3)

# list.insert(index, object)
list3.insert(6, "Mango")
print(list3)

# numlist = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
# postion = int(len(numlist) / 2)
# print(postion)
# numlist.insert(postion, 5.5)
# print(numlist)


# [1, 2, 3, 4, 5, 1, 'Mango', 'String', False, 1.5, 'Tofayel']
# print(list3)

# list.pop(index)
list3.pop(6)
print(list3)

# [1, 2, 3, 4, 5, 1, 'String', False, 1.5, 'Tofayel']
# remove(value)
list3.remove("String")
print(list3)

numlist = [3, 2, 5, 1, 4, 6]
# numlist.sort(reverse = True)
numlist.reverse()
print(numlist)

x = [1, 2, 3]
y = x.copy()
x.remove(2)
print(y)

x = [1, 2, 3]
x.clear()
print(x)

nested_list = ["Mango", ["Tofayel", [1, 2, 3]]]
print(nested_list[1][-1][1])


# Grab the X
task_list = ["Item1", ["Item2"], [1, 2, [1.4, 1.6],["John", ["Doe"], [9, [], 11, [False, True,[False, ["X"], True, 1.8], 23], 30], "Tofayel"], ["Atiq"], "Asib", "Noyon"], False]
print(task_list[2][3][2][3][2][1][-1])

list_x = [2, "Asib", False, [0, 1, 2]]
# list indexing => list_name[index_position]
print(list_x[3][1])

my_new_list = [1, 2, 1, 1, 1, 3, 4]
print(my_new_list.count(1))

# rolls = [[1, "Tofayel"], [2, "Shetu vai"], [3, "Ashik"]]
# rolls.append([4, "John"])
# print(rolls)