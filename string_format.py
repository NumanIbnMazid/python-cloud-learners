print("This is a string {}".format("INSERTED"))

name = "John Doe"
intro = "My name is {} I am {} years old".format(name, 23)
print(intro)

intro = "My name is {1} I am {0} years old".format(23, "Asib")
print(intro)

name = "Noyon"
age = 16
intro = f"My name is {name}. I am {age} years old"
print(intro)


result = 100/777
print("The result is : {r:1.2f}".format(r=result))
print("The result is : {r:10.2f}".format(r=result))