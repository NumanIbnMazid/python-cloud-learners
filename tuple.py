my_tuple = ()
print(type(my_tuple))

my_tuple_2 = (True, "String", 1.5, 23, [1, 2, 3])
print(my_tuple_2)
print(my_tuple_2[1])
print(my_tuple_2[4])

print(len(my_tuple_2))

print(my_tuple_2[1:3])

print(my_tuple_2[4][1])

my_tuple_3 = ("One", "One", "One", "Three", "Four")
print(my_tuple_3.count("One"))

print(my_tuple_3.index("Four"))
