x = 100
while x <= 110:
    print(x)
    x += 10

# Continue
mystring = "Ashiq"

print("\n continue =>")

for letter in mystring:
    if letter == "h" or letter == "i":
        continue
    print(letter)

print("\n break =>")

for letter in mystring:
    if letter == 'i':
        break
    print(letter)

print("Dynamic name =>")

mystring = "noyon"
string_len = len(mystring)
index = 0
for letter in mystring:
    # if letter == "n":
    #     break
    # print(letter)
    if index < string_len - 1 :
        print(letter)
    index += 1
    # print(index)


for letter in mystring:
    pass
      # Does nothing

for _ in [1,2,3,4]:
    print(0)
