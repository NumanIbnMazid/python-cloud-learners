# Dictionary => Key, value pair
# {key:value}
# {}
# key:value
# dict

my_dict = {
    'key1': 'value1',
    'key2': 'value2',
    'key3': 'value3',
}

my_dict2 = {
    'one': 'asib',
    'two': 'noyon'
}

print(type(my_dict2))
print(my_dict.keys())
print(my_dict.values())
print(len(my_dict))

empty_dict = {}
print(type(empty_dict))

# add item to dictionary => dictionary_name['key'] = value
empty_dict["One"] = "Mango"
print(empty_dict)

empty_dict["Two"] = "Orange"
print(empty_dict)
print(empty_dict.keys())
print(empty_dict.values())

print(empty_dict['One'])

nested_dict = {
    "one": [1, False, "X"],
    "two": "Ashiq",
    "three": 1.5,
    "four": {'first_name': "John", 'last_name': "Doe"}
}

print(nested_dict["one"][2])

# first_name = "John"
# last_name = "Doe"
# print(first_name + " " + last_name)
common = nested_dict['four']
# print(common)
print(common['first_name'] + " " + common["last_name"])

# John Doe
# print(nested_dict['four']['first_name'] + " " + nested_dict['four']['last_name'])

# nested_dict.clear()
# print(nested_dict)

# Delete
del nested_dict['one']
print(nested_dict)

# Update
# dict_name[key] = updated_value
nested_dict['two'] = "Asib"
print(nested_dict)
